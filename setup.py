#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='mymodule',
    version='0.1',
    url='https://www.palletsprojects.com/p/flask/',
    license='BSD',
    maintainer='Me',
    maintainer_email='alexsavio@gmail.com',
    description='my module',

    packages=['mymodule'],
    include_package_data=True,

 
)